# Introduction to the DaVinci software

As mentioned in previous lessons, the Brunel application is responsible for transforming the triggered, raw data from the detector into large files called DSTs, which contain the full reconstruction information of the events. For our analysis, we will be applying selections to the reconstructed events so that the data files we eventually manage are more manageable from a computing point of view. They are ROOT files, and to go from DSTs to ROOT files, we use the DaVinci software.

To produce the output ROOT files, or ntuples, we need to send DaVinci some instructions so that it can operate properly. We do this through the _options file_. Our main object of use will be the class `DecayTreeTuple`, which will create a TTree inside the ROOT file with the information we ask for. Lastly, we need our stripping line, the filter through which we will be selecting the desired candidates from our Monte Carlo DST. In our case, we will be using the stripping line `D2hhPromptDst2D2KKLine`

Let us create a new file named `ntuple_options.py`, and write:

```python
# Important imports
from Configurables import DecayTreeTuple

# Stream and stripping line where our particles are
stream = "AllStreams"
line = "D2hhPromptDst2D2KKLine"

# DecayTreeTuple definition
dtt = DecayTreeTuple("DstDKKPiTuple")
dtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
dtt.Decay  = "[D*(2010)+ -> (D0 -> K- K+) pi+]CC"
```

We now use the `DaVinci` class to set the configuration attributes. We add the following lines to `ntuple_options.py`:

```python

from Configurables import DaVinci

DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType       = "DST"
DaVinci().TupleFile       = "DVntuple.root"
DaVinci().PrintFreq       = 1000
DaVinci().DataType        = "2016"
DaVinci().Simulation      = True
DaVinci().Lumi            = not DaVinci().Simulation
DaVinci().EvtMax          = -1
DaVinci().CondDBtag       = "sim-20170721-2-vc-md100"
DaVinci().DDDBtag         = "dddb-20170721-3"
```

Here is a quick summary of these attributes:

- `UserAlgorithms` indicates the algorithm to be run over the events.
- `InputType` should be `'DST'` for DST files (and `'MDST'` for microDST).
- `TupleFile` is the name of the output ROOT file, where the TTree will be stored.
- `PrintFreq` is the frequency of events with which DaVinci will print the status.
- `DataType` is the year of data-taking this corresponds to.
- `Simulation` is either `True` when dealing with Monte Carlo files, or `False` when using LHCb-taken data.
- `Lumi` is set to `True` if we want to store information on the integrated luminosity. It is usually set to the opposite value as `Simulation`, as we are only interested in getting the integrated luminosity value for data files. This is because it is a way to check whether or not we have run over the full dataset, as we can compare this value to the integrated luminosity of each year.
- `EvtMax` is the number of events to run over, where `-1` indicates to run over all events.
- `CondDBtag` and `DDDBtag` are the exact detector conditions that the Monte Carlo was generated with. It contains, for instance, information on the magnet polarity.

---
## Database tags
Generally, the `CondDB` and `DDDB` tags are different for each dataset you
want to use, but will be the same for all DSTs within a given dataset.

For real collision data, you shouldn't specify these tags, as the default
tags are the latest and greatest, so just remove those lines from the options
file.We are now ready to run DaVinci. Going back to the terminal, to the directory where both the `ntuple_options.py` module and the `.dst` file are stored, we simply type:

```shell
$ lb-run DaVinci/v46r4 gaudirun.py ntuple_options.py
```

The full options file we've created, `ntuple_options.py`, is [available here](./code/minimal-dv/ntuple_options.py). A slightly modified version that uses remote files (using an XML catalog as [described here](files-from-grid)) is [available here](./code/minimal-dv/ntuple_options_xmlcatalog.py).

Once the DaVinci process has ended, we should have a new `DVntuple.root` file in the current directory.

## Getting more detailed information

As the script is right now, the output ROOT file will have some basic information on the mother particle, `D*`, by default. To add information on the rest of the particles on the decay, we add a `^` before each of them. The exception comes when we have a parenthesis, in which case we add it right before the parenthesis to indicate that we want information on the mother particle of the sub-decay. In other words, we simply write:

```python
dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
```

If we run the code again, we will see that we have a lot more branches on the output ROOT file. The names of the branches are automatically assigned by DaVinci, but we can rename them by hand. To do that, we first have to import `addBranches` from `DecayTreeTuple.Configuration` and then pass this function a dictionary where the keys are the desired names for the particles, and the values are the decay descriptor where we mark the particle in question. The code is as follows:

```python
from DecayTreeTuple.Configuration import addBranches

dtt.addBranches({"Dstar" : "[D*(2010)+ -> (D0 -> K- K+) pi+]CC",
                 "D0"    : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC",
                 "Kminus": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kplus" : "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pisoft": "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})
```

# TupleTools

We can add as much information to our ntuples as we like. The handling of additional information is done through `TupleTools`, which are C++ classes that help us fill in the output ROOT file. Here is a quick breakdown of some of the basic ones:

 - `TupleToolKinematic`, which fills the kinematic information of the decay;
 - `TupleToolPid`, which stores DLL and PID information of the particle;
 - `TupleToolANNPID`, which stores the new NeuralNet-based PID information of the particle;
 - `TupleToolGeometry`, which stores the geometrical variables (IP, vertex position, etc) of the particle;
 - `TupleToolEventInfo`, which stores general information (event number, run number, GPS time, etc) of the event;
 - `TupleToolTrackInfo`, which fills the tracking information of our particles;
 - `TupleToolPrimaries`, which stores the information of the PV associated to our particle.

We can add a basic set of TupleTools to our `DecayTreeTuple` object as follows:

```python
dtt.ToolList = ["TupleToolEventInfo",
	        "TupleToolANNPID",
		"TupleToolGeometry",
		"TupleToolKinematic"]
```

We can also explore further configuration of certain TupleTools. To do that, we use the method `addTupleTool`, and we have the option of storing the output in a variable, so that we can later set more attributes. For instance, we can add the TupleTool `TupleToolTrackInfo` to fill the tracking information of our particles. But additionally, we also have the option of storing further information from the tracks, such as the number of degrees of freedom of the track fit. To do that, we set the `Verbose` attribute as `True`:

```python
track_tool         = dtt.addTupleTool("TupleToolTrackInfo")
track_tool.Verbose = True
```

It is not necessary to create a new variable, though, and we can simply type the following if we wanted to keep PV information:

```python
dtt.addTupleTool("TupleToolPrimaries")
```

We can also add specific information on simply one of the particles. Consider a situation in which we wanted to store the proper time information of the mother particle, D0. We would simply type:

```python
dtt.D0.addTupleTool("TupleToolPropertime")
```

There are two big asterisks here. First of all, the structure of the variable that calls for `addTupleTool` must be `dtt.PARTICLENAME`, where `PARTICLENAME` is the exact name that is in the `addBranches` dictionary, without the string quotes. Say that the value we passed the dictionary comes from a previously defined variable, `d0name`. We can rewrite the above line as follows:

```python
### ALTERNATIVE OPTION ###
d0name = "D0"
getattr(dtt, d0name).addTupleTool("TupleToolPropertime")
```

Both options are completely equivalent. 

The second asterisk is the fact that this TupleTool may raise an error if the D0 does not come from the PV. We have to ensure that this is the case, and for that we change the `UserAlgorithms` line to this:

```python
from Configurables import CheckPV
DaVinci().UserAlgorithms += [CheckPV(), dtt]
```

There is another, more sophisticated approach to ask for the Primary Vertex. The `CheckPV()` function should work in most cases, but at the end of the day, it's another black box. Alternatively, we can use a `LoKi__VoidVilter` object and ask for the events to contain a specific TES location:

```python
### ALTERNATIVE OPTION ###
from Configurables import LoKi__VoidFilter

pv = LoKi__VoidFilter("hasPV",Code="CONTAINS('Rec/Vertex/Primary')>0")
DaVinci().UserAlgorithms += [pv, dtt]
```

Before we move on, one final note. It is possible that, under this set up, there are events that fail to meet the filter but go on to apply the `DecayTreeTuple` algorithms, and therefore crash when adding `TupleToolPropertime`. To make sure that the algorithms we want to run are processed sequentially, we can use a `GaudiSequencer`. It's quite easy to use:

```python
from Configurables import LoKi__VoidFilter, GaudiSequencer

pv = LoKi__VoidFilter("hasPV", Code="CONTAINS('Rec/Vertex/Primary')>0")
gs = GaudiSequencer("myseq")
gs.Members += [pv, dtt]

DaVinci().UserAlgorithms += [gs]
```

---
## Where to find TupleTools

One of the most difficult things is to know which tool we need to add to our 
`DecayTreeTuple` in order to get the information we want.
For this, it is necessary to know where to find `TupleTools` and their code.
`TupleTools` are spread in 9 packages under `Analysis/Phys` (see the master branch in `git` [here](https://gitlab.cern.ch/lhcb/Analysis/tree/run2-patches/Phys)), all starting with the prefix `DecayTreeTuple`, according to the type of information they fill in our ntuple:

- `DecayTreeTuple` for the more general tools;
- `DecayTreeTupleANNPID` for the NeuralNet-based PID tools;
- `DecayTreeTupleDalitz` for Dalitz analysis;
- `DecayTreeTupleJets` for obtaining information on jets;
- `DecayTreeTupleMC` gives us access to MC-level information;
- `DecayTreeTupleMuonCalib` for muon calibration tools;
- `DecayTreeTupleReco` for reconstruction-level information, such as `TupleToolTrackInfo`;
- `DecayTreeTupleTracking` for more detailed tools regarding tracking;
- `DecayTreeTupleTrigger` for accessing to the trigger information of the candidates.

The `TupleTools` are placed in the `src` folder within each package and it's usually easy to get what they do just by looking at their name.
However, the best way to know what a tool does is check its documentation, either by opening its `.h` file or be searching for it in the latest [`doxygen`](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/index.html).
Most tools are very well documented and will also inform you of their configuration options.
As an example, to get the information on the `TupleToolTrackInfo` we used before we could either check its [source code](https://gitlab.cern.ch/lhcb/Analysis/blob/run2-patches/Phys/DecayTreeTupleReco/src/TupleToolTrackInfo.h) or its [web documentation](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/da/ddd/class_tuple_tool_track_info.html).
 In case we need more information or need to know *exactly* what the code does, the `fill` method is the one we need to look at.

 As a shortcut, the list of tupletools can also be found in doxygen at the top of the pages for the [`IParticleTupleTool`](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/de/df8/struct_i_particle_tuple_tool.html) and the [`IEventTupleTool`](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/d5/d88/struct_i_event_tuple_tool.html) interfaces (depending on whether they fill information about specific particles or the event in general).

---

# LoKi Functors

LoKi functors allow us to obtain even more specific information from the DST. If you've ever seen the details on how a stripping line is configured, you may have already come across these objects. (You can access the configuration to all the stripping lines [on this link](https://lhcbdoc.web.cern.ch/lhcbdoc/stripping/)) They are our language to interact with the DST and apply cuts before we introduce them into our output ROOT file. Roughly speaking, LoKi functors are to DSTs what Branches are to ROOT files. It is important to realize, however, that not all LoKi functors have a direct translation into branches in our ROOT files, which can sometimes be the source of big headaches.

Let's say, for instance, that we are interested in adding a variable to the ROOT file that includes the minimum momentum of the daughters. To do that, we simply have to create our own TupleTool and add a specific variable that we create through the use of LoKi functors. Here is an example where we add both the minimum and maximum momentum of the daughters:

```python
mainTools = dtt.D0.addTupleTool("LoKi::Hybrid::TupleTool/MyCustomVars")
mainTools.Variables = {
        "MinDght_P": "PFUNA(AMINCHILD(P))",
        "MaxDght_P": "PFUNA(AMAXCHILD(P))",
}
```

There are three LoKi functors at play here. The `P` functor collects the momentum of the particles, the `AMINCHILD()` (or `AMAXCHILD()`) sets the minimum (or maximum) of the given functor for the daughter particles. Finally, `PFUNA` is a bit more technical, but essentially allows for the conversion between the array-like comparison that `AMINCHILD()` performs and the output variable. As previously seen, the keys of the dictionary are the names of the branches on the output ROOT file.

## Much more on LoKi functors to come

Working with LoKi functors is an essential part of dealing with ntuple-making and DaVinci. You can find some documentation about what each of them does [on this link](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/d7/dae/namespace_lo_ki_1_1_cuts.html). The lesson on the official Starterkit page, [`Fun with LoKi functors`](https://lhcb.github.io/starterkit-lessons/first-analysis-steps/loki-functors.html), deals with this a bit differently. They use it to interact directly with the DST file and read off values from the actual events. It is a more intuitive approach as it deals directly with the original data, but I prefered not to include it to make the lessons be less confusing and more focused. 

LoKi functors are an essential part of applying stripping cuts at the DST level, and they will come back in slightly more advanced parts of DaVinci.

# Using Catalogs

Even though we downloaded our DST files in our previous session to have quicker access to our data and test our DaVinci conde, in general, there is no need to get our hands dirty downloading DST files. This can be especially tedious and space-consuming when working with multiple decays and multiple years. We can always access DST files directly from the grid through DaVinci, by using catalogs.

Let us first create a directory called `catalogs`, and move our `*ALLSTREAMS.DST.py` file that we downloaded from DIRAC in there. We will create a copy that we call `testcatalog.py`, open it and delete all LFNs except for the first two, which we will use for our testing.

After that, we go back to the terminal and run the following command:

```
$ lb-dirac dirac-bookkeeping-genXMLCatalog --Options=testcatalog.py --Catalog=myCatalog.xml
```

We can now go into our options file and substitute the `IOHelper` line(s) with the following two lines:

```python
from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs = [“xmlcatalog_file:catalogs/mycatalog.xml”]
```

# DecayTreeFitter

In same specific cases it can be very useful to do a refit of our decay chain considering new knowledge, which can be obtained while doing the analysis. This new knowledge is represented as constraints which your decay tree has to fulfill. The result of this refit will lead to a better estimation for the particle parameters – in particular their momenta, which can imply in an improvement of our decay reconstruction and in a cleaner invariant mass spectrum. To see examples of this constraints, let's consider the decay

``'[D*(2010)+ -> (D0 -> K- K+) pi+]CC'``

you can make the assumption that the (K- K+) combine to form a D0 with a specific invariant mass. This results in a so called *mass constraint*. In addition the two kaons should originate from exactly the same point in space. If you know that your data only contains prompt D* candidates, you can constrain them to do come from the primary vertex. Boundary conditions like those are called vertex constraints (the last of which is known as a primary-vertex constraint).

In order to do this kinematic refit, there is an algorithm call `DecayTreeFitter` that does the task for us. This algorithm is included in the TupleTool `TupleToolDecayTreeFitter` an in order to use it we have to added to the head of the decay as (for the D*):

```python
dtt.Dstar.addTupleTool('TupleToolDecayTreeFitter/ConsD')
```

Now we can proceed with the configuration of the fitter. We are going to constrain the D0 to have originated from the primary vertex. We want all the output available, so we set the verbose option. Finally we want to apply the mass constraint to the D0:

```python
dtt.Dstar.ConsD.constrainToOriginVertex = True
dtt.Dstar.ConsD.Verbose = True
dtt.Dstar.ConsD.daughtersToConstrain = ['D0']
```


Note that you can constrain more than one intermediate state at once if that fits your decay. When using the this TupleTool, all the variables created by the other TupleTools are not affected by the change, but some new variables are created, one set per `DecayTreeFitter` instance. Depending on whether the `Verbose` option is specified, the new variables are created for the head particle only or for the head particle and its decay products too.

If the decay products are not stable particles and decay further, the decay products of the decay products have no new variables associated to them by default. Since in many cases this information might be useful, there is an option to store the information from those tracks

```python
dtt.Dstar.ConsD.UpdateDaughters = True
```

After adding this changes to our option file and run it, the resulting nTuple will have several new branches with `ConsD` in the name. Most of this branches will correspond to array variables because, in general, there is more than one vertex hypothesis. Therefore, the `DecayTreeFitter` will use each of them to do the fit and the resulting mass will be stored in this array variables. If we want to get the refitted mass that corresponds the best PV hypothesis, we can just plotted like (in ROOT):

```C++
tree->Draw("Dstar_ConsD_M[0]>>h(200,2000,2030)","","");
```

Finally, there is an alternative way of applying a `DecayTreeFitter` algorithm through LoKi Functors. For that we have to use the `DTF_FUN` functor in `LoKi::Hybrid::TupleTool`. The advantage of this method is that it allows to customize the output variables as shown below


```python
LoKi_DTFFun = dtt.Dstar.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_DTFFun")
LoKi_DTFFun.Variables = {
    "DTFFun_Dstart_P"   : "DTF_FUN(P, True, 'D0')",
    "DTFFun_Dstar_PT"   : "DTF_FUN(PT, True, 'D0')",
    "DTFFun_Dstar_M"    : "DTF_FUN(M, True, 'D0')",
    "DTFFun_DTF_CHI2"   : "DTF_CHI2(True, 'D0')",
    "DTFFun_DTF_NDOF"   : "DTF_NDOF(True, 'D0')",
    "DTFFun_D0_M"       : "DTF_FUN(CHILD(M,  '[D*(2010)+ ->^D0 pi+]CC'), True, 'D0')",
    "DTFFun_D0_PE"      : "DTF_FUN(CHILD(E,  '[D*(2010)+ ->^D0 pi+]CC'), True, 'D0')",
    "DTFFun_D0_PX"      : "DTF_FUN(CHILD(PX, '[D*(2010)+ ->^D0 pi+]CC'), True, 'D0')",
    "DTFFun_D0_PY"      : "DTF_FUN(CHILD(PY, '[D*(2010)+ ->^D0 pi+]CC'), True, 'D0')",
    "DTFFun_D0_PZ"      : "DTF_FUN(CHILD(PZ, '[D*(2010)+ ->^D0 pi+]CC'), True, 'D0')"
}
```

The first argument of `DTF_FUN` is the LoKi functor that defines the output variable. The second (boolean) argument defines if primary-vertex constraint is required or not. The third argument is optional and specifies a particle or list of particles to be mass-constrained. In the case of multiple mass constraints this argument should look like `strings(['particle1','particle2'])`. The quality of the fit can be accessed by `DTF_CHI2` functor.

The `DecayTreeFitter` implementation described above has a disadvantage that it will re-run the fit for every variable requested by `DTF_FUN`. A more efficient and __strongly recommended way__ to use LoKi-based DecayTreeFitter can be done using `LoKi__Hybrid__Dict2Tuple` tool as described in DaVinci tutorial.


# MCDecayTreeTuples

As we've seen in the Dataflow for Run 2 lesson, the first step of the Monte Carlo production is the particle simulation done under the Gauss framework. This is the most basic step of the process, as it does not involve the LHCb detector whatsoever. We can actually access the information generated here and create a new TTree object on our output ROOT file. 

Why would we be interested in this? In one word: Efficiencies! Getting a handle of our efficiencies in the analysis is absolutely key, and it might also be useful to expand them and know the efficiency of each step of the process (reconstruction, stripping, trigger, etc.) Let's say we want to compute the stripping efficiency. We need to know how many events have passed the stripping line over how many events we had in total. The <em>total number of events</em> is given by the `MCDecayTreeTuple`. Here is how to implement it:

```python
from Configurables                import MCDecayTreeTuple
from DecayTreeTuple.Configuration import addBranches
    
mctuple        = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay  = "[D*(2010)+ => (D0 ==> K- K+) pi+]CC"
## Add here further configuration (branch names, TupleTools, etc.)

DaVinci().UserAlgorithms += [mctuple]
```

Alternatively, if we are using a GaudiSequencer, this would be the first item on the sequence. Notice also that the decay descriptor is now using the 'newer' LoKi decay descriptor syntax. You can check out more information [here](https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders).

# Running on mDST data

As we saw, mostly of the MC made in LHCb is stored in DSTs, which have all the event information stored. In Run1 it was the prefered file for data storage, but it changed for Run2. With the increase of energy of the LHC there was an increase of the number of interesting events, so more data had to be stored. To tackle this problem the mDST format was introduced. Unlike the DSTs, in the mDST not all the event information is stored and only stripping level information is stored. In other words, only particles and selections of each stripping line of the stream is stored and the rest of the information is erased. This leads to a huge reduce of the data stored and only important information is stored (by the end we only work with final decay reconstructions). But this is a problem for those who are interested in global event information.

Apart from this, working with mDST involves another problems. Basically most of the TES locations are preserved but the stream change and it will depend on which working group made the mDST files. Therefore, some changes have to made to our options files. Let's start from the definition of the `DecayTreeTuple` for DST files

```python
# Important imports
from Configurables import DecayTreeTuple

# Stream and stripping line where our particles are
stream = "AllStreams"
line = "D2hhPromptDst2D2KKLine"

# DecayTreeTuple definition
dtt = DecayTreeTuple("DstDKKPiTuple")
dtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
dtt.Decay  = "[D*(2010)+ -> (D0 -> K- K+) pi+]CC"
```

For mDST file we have to do the following changes


```python
# Important imports
from Configurables import DecayTreeTuple

# Stream and stripping line where our particles are
stream = "Charm"
line = "D2hhPromptDst2D2KKLine"

# DecayTreeTuple definition
dtt = DecayTreeTuple("DstDKKPiTuple")
dtt.Inputs = ["/Phys/{0}/Particles".format(line)]
dtt.Decay  = "[D*(2010)+ -> (D0 -> K- K+) pi+]CC"
dtt.RootInTES = "/Event/{0}".format(stream)
```

We have to remove from the `Inputs` method the `/Event/AllStreams/` location and we have to include it through the new `RootInTES` method. This is made to give DaVinci the good location of the particles since for the mDST the TES structure is different. In this case, the `D2hhPromptDst2D2KKLine` stripping line belongs to Charm WG. For this WG, the name of the stream is `Charm`. For other WG has to be checked by the analyst. 

# Build your own decay: Selection Framework

Have your ever check what your stripping line does?. If the answer is negative, we can check it by going to this link [web documentation](https://lhcbdoc.web.cern.ch/lhcbdoc/stripping/). If the answer is positive, probably you saw several lines that are difficult to understand, but it does the job in the end :). The point is that, in order to reconstruct a decay chain, you have to do a sequence of particle combinations and selections that allows you to get you desire resonances (for example, you can combine two kaons in the $\phi$ window or in the $D^0$ one). The machinary to perform this process is the __Selection Framework__, which is basically what a stripping line uses to the do a decay chain reconstruction.

This framework is constituted by several algorithms that allow you to pick the final state particles (kaons, pions, etc) through something called _containers_, combine them through a kinematic fit and apply several selections to the daughters, the combination and the mother. This is all made at LoKi functor level and, as it was said, is the base of the stripping process. The thing is that, the DaVinci framework allows you to use the Selection Framework in your options file. This is a powerfull tool because allows you to create new decays reconstruction that do not have a stripping line asociated, or even slightly modify a stripping line that do not suit perfectly your analysis. Something important to emphasise is that this tool will work better for DST files since all particle produced in the collisions are stored. For mDST only stripping line particles are stored, so when running the Selection Framework over them you have to be really carefull with the cuts that were made

Once explained, let's do an example of how we can add this to our option file. We are going to reconstruct our `D*(2010)+ -> (D0 -> K- K+) pi+` decay through this framework. Firstly we are going to import the particle container that correspond to pions and kaons. There are plenty of them ([web documentation](https://gitlab.cern.ch/lhcb/Phys/-/tree/run2-patches/Phys/CommonParticles/python/CommonParticles?ref_type=heads)) and they perform different tasks for each LHCb particle, but we are going to use the following ones

```python
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons as Kaons
```

The first step of our decay is to perform the `(D0 -> K- K+)` combination. For doing the combination, we use the `CombineParticles` class that allows us to add several selections to the fitting process. We list here remarkable arguments of this class:

- `DaughtersCuts` is a dictionary that maps each child particle type to a LoKi particle functor that determines if that particular particle satisfies our selection criteria. Optionally, one can specify also a `Preambulo` property that allows us to make imports, preprocess functors, etc. For example:

```python
d0_decay_products = {
  'K-': '(PT > 750*MeV) & (P > 4000*MeV) & (MIPCHI2DV(PRIMARY) > 4)',
  'K+': '(PT > 750*MeV) & (P > 4000*MeV) & (MIPCHI2DV(PRIMARY) > 4)'
}
```

- `CombinationCut` is a particle array LoKi functor (note the `A` prefix) that is given the array of particles in a single combination (the children) as input (in our case two kaons). This cut is applied before the vertex fit so it is typically used to save CPU time by performing some sanity cuts such as `AMAXDOCA` or `ADAMASS` before the CPU-consuming fit:

```python
d0_comb = "(AMAXDOCA('') < 0.2*mm) & (ADAMASS('D0') < 100*MeV)"
```

- `MotherCut` is a selection LoKi particle functor that acts on the particle produced by the vertex fit (the parent) from the input particles, which allows to apply cuts on those variables that require a vertex, for example

```python
# We can split long selections across multiple lines
d0_vertex = (
  '(VFASPF(VCHI2/VDOF)< 9)'
  '& (BPVDIRA > 0.9997)'
  "& (ADMASS('D0') < 70*MeV)"
)
```

In order to call the `CombinationParticle` class, we have to add the following.

```python
from Configurables import CombineParticles
d0 = CombineParticles(
    'Combine_D0',
    DecayDescriptor='[D0 -> K- K+]cc',
    DaughtersCuts=d0_decay_products,
    CombinationCut=d0_comb,
    MotherCut=d0_vertex
)
```

The next step is to build a `Selection`, which basically is a class that merges the `CombineParticles` algorithm with the particle containers

```python

from PhysConf.Selections import Selection
d0_sel = Selection(
    'Sel_D0',
    Algorithm=d0,
    RequiredSelections=[Kaons]
)
```

Since `(D0 -> K- K+)` decay only has kaons, we only use the kaons container. Once this selection is made, the next step is to do the `D*(2010)+ -> (D0 -> K- K+) pi+` combination. Again, we have use the previous algorithms as it is shown in the following lines (we add also some cuts)

```python
dstar_decay_products = {'pi+': '(TRCHI2DOF < 3) & (PT > 100*MeV)'}
dstar_comb = "(ADAMASS('D*(2010)+') < 400*MeV)"
dstar_vertex = (
    "(abs(M-MAXTREE('D0'==ABSID,M)-145.42) < 10*MeV)"
    '& (VFASPF(VCHI2/VDOF)< 9)'
)

dstar = CombineParticles(
    'Combine_Dstar',
    DecayDescriptor='[D*(2010)+ -> D0 pi+]cc',
    DaughtersCuts=dstar_decay_products,
    CombinationCut=dstar_comb,
    MotherCut=dstar_vertex
)

dstar_sel = Selection(
    'Sel_Dstar',
    Algorithm=dstar,
    RequiredSelections=[d0_sel, Pions]
)
```

Here for the `D*` we need two particles, the D0 and a pion. We got the pions from the container while the D0 comes from the `Selection` that was previously defined. Once all this process is made, we have to add this to our `DecayTreeTuple` algorithm. For that, we have to create a _sequence_, which is basically a way to order the steps of the decay reconstruction (clearly we want to fit first the D0 and after de D*). For that, we use the following class

```python
from PhysConf.Selections import SelectionSequence
dstar_seq = SelectionSequence('Dstar_Seq', TopSelection=dstar_sel)
```

Now we can proceed to define the `DecayTreeTuple`. However, some changes to algorithm are needed:

```python
from Configurables import DecayTreeTuple, DaVinci, LoKi__VoidFilter, GaudiSequencer
from DecayTreeTuple.Configuration import addBranches
from GaudiConf import IOHelper

# DecayTreeTuple definitions
dtt = DecayTreeTuple("DstDKKPiTuple")

# Now there is not stripping line. For inputs, we have use the following
dtt.Inputs = dstar_seq.outputLocations()
dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
dtt.addBranches({"Dst": "[D*(2010)+ -> (D0 -> K- K+) pi+]CC", 
                 "Dz" : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC", 
                 "Kmi": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kpl": "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pi" : "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})

# TupleTools addition
tupletools_list =  ["TupleToolEventInfo",   
                    "TupleToolANNPID",      
                    "TupleToolGeometry",    
                    "TupleToolKinematic"]  

dtt.ToolList = tupletools_list

gs = GaudiSequencer()
gs.Members += [dstar_seq.sequence(), dtt]

#=============================================================================================================================================

# DaVinci options
DaVinci().UserAlgorithms += [gs]                # Instead of the DecayTreeTuple, we put the sequencer
DaVinci().InputType  = "DST"                    # DST MC
DaVinci().DataType   = "2016"                   # Year
DaVinci().EvtMax     = -1
DaVinci().CondDBtag  = "sim-20170721-2-vc-md100" 
DaVinci().DDDBtag    = "dddb-20170721-3"
DaVinci().TupleFile  = "advanced_ntuple.root"       # Name of the ntuple 
DaVinci().PrintFreq  = 1000                         # Pritting options
DaVinci().Simulation = Simulation                   # MC or Data 
DaVinci().Lumi       = not DaVinci().Simulation     # Only True for Data


# Read local nTuples
dst_path  = "../DST_files/"
dst_name  = "00070793_00000040_7.AllStreams.dst"

IOHelper().inputFiles([dst_path+dst_name], 
                       clear=True)
```

Basically the `Input` value will come from the `SelectionSequence` (which is logic because we are doing the selection on real time. This things are not stored in the DST) and we used a `GaudiSequencer` to indicate that, firstly we have to run the Selection Framework process (`dstar_seq.sequence()`) and after the `DecayTreeTuple` algorithm.