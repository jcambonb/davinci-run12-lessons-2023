# DaVinci Run12 Lessons 2023


## Introduction

This repository constains the materials for the introductory lessons to the DaVinci software for the LHCb taught during the [LHCb Starterkit in Febreary 2024](https://indico.cern.ch/event/1337166/). They are mostly based on the lessons from the [First Analysis Steps](https://lhcb.github.io/starterkit-lessons/first-analysis-steps/README.html), the last [LHCb Starterkit DaVinci Run1+2](https://indico.cern.ch/event/1206471/timetable/?view=standard) made by [Miguel Fernandez](https://gitlab.cern.ch/femiguel/davinci-lessons/-/tree/master?ref_type=heads) and same personal additions. The materials included are the following

- `dv_run12_step_by_step.md`, which is a guide of the lesson. It explains more in detail each step that are used in the codes

- `basic_codes`, which includes codes to perform our first DaVinci option files. Each code will be a topic of the lesson and the order to check them is the following

    - `dv_run12_script_basic.py`
    - `dv_run12_script_branches.py`
    - `dv_run12_script_TupleTools.py`
    - `dv_run12_script_LoKi.py`
    - `dv_run12_script_gs.py`
 
- `advanced_codes`. which includes codes to perform more advanced tasks in our DaVinci option files. If we have time enough we will see some of them in the lesson. If not, it remains as material for those interested in something more advance. The order to check them is the following.

    - `dv_run12_script_catalog.py`
    - `dv_run12_script_dtf.py`
    - `dv_run12_script_dtf_LoKi.py`
    - `dv_run12_script_mcdtt.py`
    - `dv_run12_script_mdst.py`
    - `dv_run12_script_trigger_info.py`
    - `dv_run12_script_sel_fr.py`


## Requirements

In order to run this codes, our machine has to have the `LbEnv` installed and compiled. Moreover, the DaVinci version that we are going to use will be the `v46r4` (last for Run2). For the StarterKit class is highly recommended to use `lxplus` which has everything that we need. In addition, we are going to run our options files over a DST of the $D^{*+} \to D^0(\to K^+ K^-) \pi^+$ MC and a mDST of 2018 Charm data. Both of them can be founded in the following path of `lxplus`

```python
"/afs/cern.ch/user/j/jcambonb/public/DaVinci_Run12_Lessons_2023/DST_files/"
```