#-- GAUDI jobOptions generated on Tue Jan  9 16:41:30 2024
#-- Contains event types : 
#--   27163002 - 688 files - 1750979 events - 386.96 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged' 

#--  StepId : 132434 
#--  StepName : Digi14b for 2015 - 25ns spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v30r2p1 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/DataType-2015.py;$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r338 
#--  Visible : N 

#--  Processing Pass: '/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged' 

#--  StepId : 130088 
#--  StepName : L0 emulation for 2016 - TCK 0x160F - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v25r4 
#--  OptionFiles : $APPCONFIGOPTS/L0App/L0AppSimProduction.py;$APPCONFIGOPTS/L0App/L0AppTCK-0x160F.py;$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py;$APPCONFIGOPTS/L0App/DataType-2016.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r297 
#--  Visible : N 

#--  Processing Pass: '/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged' 

#--  StepId : 130089 
#--  StepName : TCK-0x5138160F (HLT1) Flagged for 2016 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v25r4 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x5138160F.py;$APPCONFIGOPTS/Moore/DataType-2016.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r297 
#--  Visible : N 

#--  Processing Pass: '/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged' 

#--  StepId : 130090 
#--  StepName : TCK-0x6139160F (HLT2) Flagged for 2016 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v25r4 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x6139160F.py;$APPCONFIGOPTS/Moore/DataType-2016.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt2.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r297 
#--  Visible : Y 

#--  Processing Pass: '/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged' 

#--  StepId : 130615 
#--  StepName : Reco16 for MC 2016 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v50r2 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2016.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r314;Det/SQLDDDB.v7r10 
#--  Visible : Y 

#--  Processing Pass: '/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged' 

#--  StepId : 131792 
#--  StepName : Turbo lines (MC), Turbo 2016 - Stripping28 - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v41r4p3 
#--  OptionFiles : $APPCONFIGOPTS/Turbo/Tesla_2016_LinesFromStreams_MC.py;$APPCONFIGOPTS/Turbo/Tesla_PR_Truth_2016.py;$APPCONFIGOPTS/Turbo/Tesla_Simulation_2016.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r322;TurboStreamProd.v4r1p4 
#--  Visible : Y 

#--  Processing Pass: '/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged' 

#--  StepId : 132983 
#--  StepName : Stripping28r1-NoPrescalingFlagged for Sim09 - pp at 13 TeV - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v41r4p4 
#--  OptionFiles : $APPCONFIGOPTS/DaVinci/DV-Stripping28r1-Stripping-MC-NoPrescaling-DST.py;$APPCONFIGOPTS/DaVinci/DataType-2016.py;$APPCONFIGOPTS/DaVinci/InputType-DST.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r348;TMVAWeights.v1r9 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/2016/ALLSTREAMS.DST/00070793/0000/00070793_00000040_7.AllStreams.dst',
'LFN:/lhcb/MC/2016/ALLSTREAMS.DST/00070793/0000/00070793_00000031_7.AllStreams.dst',
], clear=True)
