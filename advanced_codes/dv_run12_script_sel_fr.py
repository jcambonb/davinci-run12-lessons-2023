# Important imports
"""
StarterKit 2023: DaVinci Run(1/2) Lessons 

Use the selection framework to build your own decay (classical way)
We will build the Dst+ -> D0(->K+K-) without using the stripping line
The Dst+ -> D0(-> K+K-) pi+ MC DST is used
A 2018 data mDST from CHARM stream is used

Ivan Cambon Bouzas
Instituto Galego de Altas Enerxias (IGFAE)
Universidade de Santiago de Compostela

LHCb Collaboration
"""


"""
For doing the particle combination, the basic scheme is the following
- Get the particles
- Use a combiner (CombineParticles function)
- Define a selection (Selection function)
- Create a sequence
"""

# Selection Framework
#======================================================================================================================================================================
# We get the particles from the standard containers
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons as Kaons

from PhysConf.Selections import AutomaticData

Pions = AutomaticData('Phys/StdAllNoPIDsPions/Particles')
Kaons = AutomaticData('Phys/StdAllLooseKaons/Particles')

# First we define the D0->K+K- combination
# We can apply cut to the daughters, to the combination and to the vertex
# For that we use LoKi functors
d0_decay_products = {'K-': '(PT > 750*MeV) & (P > 4000*MeV) & (MIPCHI2DV(PRIMARY) > 4)',
                     'K+': '(PT > 750*MeV) & (P > 4000*MeV) & (MIPCHI2DV(PRIMARY) > 4)'}

d0_comb = "(AMAXDOCA('') < 0.2*mm) & (ADAMASS('D0') < 100*MeV)"

d0_vertex = ('(VFASPF(VCHI2/VDOF)< 9)'
             '& (BPVDIRA > 0.9997)'
             "& (ADMASS('D0') < 70*MeV)")

from Configurables import CombineParticles

# We do the combination
d0 = CombineParticles('Combine_D0',                           # Name
                      DecayDescriptor='[D0 -> K- K+]cc',      # new DecayDescriptor
                      DaughtersCuts=d0_decay_products,      
                      CombinationCut=d0_comb,
                      MotherCut=d0_vertex
                     )   # Combine selection does vertex fit. For neutral, we have to add ParticleCombiners = {"" : "MomentumCombiner:PUBLIC"}


from PhysConf.Selections import Selection

# We do the selection
d0_sel = Selection('Sel_D0',
                    Algorithm=d0,               # Here we put the combiner
                    RequiredSelections=[Kaons]  # Here we put the particles. 
                    )


# Now we do the some procedure for the Dst -> D0 pi+ combination
dstar_decay_products = {'pi+': '(TRCHI2DOF < 3) & (PT > 100*MeV)'}

dstar_comb = "(ADAMASS('D*(2010)+') < 400*MeV)"

dstar_vertex = ("(abs(M-MAXTREE('D0'==ABSID,M)-145.42) < 10*MeV)"
                '& (VFASPF(VCHI2/VDOF)< 9)')

dstar = CombineParticles('Combine_Dstar',
                         DecayDescriptor='[D*(2010)+ -> D0 pi+]cc',
                         DaughtersCuts=dstar_decay_products,
                         CombinationCut=dstar_comb,
                         MotherCut=dstar_vertex
                        )

dstar_sel = Selection('Sel_Dstar',
                      Algorithm=dstar,
                      RequiredSelections=[d0_sel, Pions] # Here we have two particles, pions and the D0 (represeted by the Selection made)
                     )

# Sequention of selection definition
from PhysConf.Selections import SelectionSequence
dstar_seq = SelectionSequence('Dstar_Seq', TopSelection=dstar_sel)
#======================================================================================================================================================================

# DecayTreeTuple definition
#======================================================================================================================================================================
from Configurables import DecayTreeTuple, DaVinci, LoKi__VoidFilter, GaudiSequencer, MCDecayTreeTuple
from DecayTreeTuple.Configuration import addBranches
from GaudiConf import IOHelper

Simulation = True

wg = "Charm"

# DecayTreeTuple definitions
dtt = DecayTreeTuple("DstDKKPiTuple")

# Now there is not stripping line. For inputs, we have use the following
dtt.Inputs = dstar_seq.outputLocations()
dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
dtt.addBranches({"Dst": "[D*(2010)+ -> (D0 -> K- K+) pi+]CC", 
                 "Dz" : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC", 
                 "Kmi": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kpl": "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pi" : "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})

# TupleTools addition
tupletools_list =  ["TupleToolEventInfo",   
                    "TupleToolANNPID",      
                    "TupleToolGeometry",    
                    "TupleToolKinematic"]  

dtt.ToolList = tupletools_list

track_tool = dtt.addTupleTool("TupleToolTrackInfo")
track_tool.Verbose = True 

if Simulation:
    MCTruth = dtt.addTupleTool("TupleToolMCTruth")  
    MCTruth.addTupleTool("MCTupleToolHierarchy")    

# LoKi functors addition
dtt_Dst_LoKi = dtt.Dst.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Dst_LoKi")
dtt_Dst_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"}

dtt_Dz_LoKi = dtt.Dz.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Dz_LoKi")
dtt_Dz_LoKi.Variables = {"ETA": "ETA",
                         "PHI": "PHI"}

dtt_Kpl_LoKi = dtt.Kpl.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Kpl_LoKi")
dtt_Kpl_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"}

dtt_Kmi_LoKi = dtt.Kmi.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Kmi_LoKi")
dtt_Kmi_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"} 

dtt_pi_LoKi = dtt.pi.addTupleTool("LoKi::Hybrid::TupleTool/dtt_pi_LoKi")
dtt_pi_LoKi.Variables = {"ETA": "ETA",
                         "PHI": "PHI"}

Dz_ct  = dtt.Dz.addTupleTool("TupleToolPropertime")

# DecayTreeFitter
dtt.Dst.addTupleTool('TupleToolDecayTreeFitter/ConsD')
dtt.Dst.ConsD.constrainToOriginVertex = True            # Vertex contrain
dtt.Dst.ConsD.Verbose                 = True            # All the information
dtt.Dst.ConsD.daughtersToConstrain    = ["D0"]          # Do a constraint to D0 -> K+K- fit
dtt.Dst.ConsD.UpdateDaughters         = True            # Update the tracks for all daughters

# Trigger information
trigger_list = [#L0                
                "L0HadronDecision",
                "L0ElectronDecision",
                "L0PhotonDecision",
                #Hlt1 
                "Hlt1TrackMVADecision",
                "Hlt1TwoTrackMVADecision",
                "Hlt1TrackMVATightDecision",
                "Hlt1TwoTrackMVATightDecision",
                #Hlt2 lines TCKsh getHlt2(0x21751801)
                "Hlt2Topo2BodyDecision",
                "Hlt2Topo3BodyDecision",
                "Hlt2Topo4BodyDecision",]


dtt_trigger = dtt.addTupleTool("TupleToolTrigger")
dtt_trigger.Verbose = True
dtt_trigger.TriggerList = trigger_list

dtt_TISTOS = dtt.addTupleTool("TupleToolTISTOS")
dtt_TISTOS.Verbose = True
dtt_TISTOS.TriggerList = trigger_list

# Now we have to define the following GaudiSequencer 
# DaVinci needs run the selection framework before the DecayTreeTuple
gs1 = GaudiSequencer()
gs1.Members += [dstar_seq.sequence(), dtt]

# mDST requirement
if not Simulation:
    gs1.RootInTES = "/Event/{0}".format(wg)

# PV vertex check
if Simulation:
    pv = LoKi__VoidFilter("hasPV", Code="CONTAINS('Rec/Vertex/Primary')>0")
    gs2 = GaudiSequencer("myseq")
    gs2.Members += [pv, gs1]
#=============================================================================================================================================

# DaVinci options
if Simulation:
    DaVinci().UserAlgorithms += [gs2]                # Instead of the DecayTreeTuple, we put the sequencer
    DaVinci().InputType  = "DST"                    # DST MC
    DaVinci().DataType   = "2016"                   # Year
    DaVinci().EvtMax     = -1
    # Magnet Conditions 
    DaVinci().CondDBtag  = "sim-20170721-2-vc-md100" 
    DaVinci().DDDBtag    = "dddb-20170721-3"
else:
    DaVinci().UserAlgorithms += [gs1]
    DaVinci().InputType  = "MDST"                   # mDST data
    DaVinci().DataType   = "2018"                   # Year
    DaVinci().EvtMax     = 50000                    # For data, -1 is to much for an easy check
    # Magnet Conditions 
    DaVinci().CondDBtag  = "cond-20180202"          # The magnet properties changed because of 2018 year and data condition
    DaVinci().DDDBtag    = "dddb-20171030-3"

DaVinci().TupleFile  = "advanced_ntuple.root"       # Name of the ntuple 
DaVinci().PrintFreq  = 1000                         # Pritting options
DaVinci().Simulation = Simulation                   # MC or Data 
DaVinci().Lumi       = not DaVinci().Simulation     # Only True for Data


# Read local nTuples
dst_path  = "../DST_files/"
dst_name  = "00070793_00000040_7.AllStreams.dst"
mdst_name = "00077434_00001776_1.charm.mdst"

if Simulation:
    IOHelper().inputFiles([dst_path+dst_name], 
                       clear=True)
else:
    IOHelper().inputFiles([dst_path+mdst_name], 
                       clear=True)

