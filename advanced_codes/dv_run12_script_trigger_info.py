"""
StarterKit 2023: DaVinci Run(1/2) Lessons 

Add trigger line information to your nTuples
The Dst+ -> D0(-> K+K-) pi+ MC DST is used
A 2018 data mDST from CHARM stream is used

Ivan Cambon Bouzas
Instituto Galego de Altas Enerxias (IGFAE)
Universidade de Santiago de Compostela

LHCb Collaboration
"""

# Important imports
from Configurables import DecayTreeTuple, DaVinci, LoKi__VoidFilter, GaudiSequencer, MCDecayTreeTuple
from DecayTreeTuple.Configuration import addBranches
from GaudiConf import IOHelper


"""
*Note*: The change that we will make are thought for mDST data.
        If the data type is DST, no changes are required (maybe stream value, but that is analysis dependent)
"""

# These bool will allow us to chose if we run the script o Data files or MC files
Simulation = False

# Stream and stripping line where our particles are
stream = "AllStreams"
line = "D2hhPromptDst2D2KKLine"    # We have to check if this stripping line is in the 2018 charm stream. Clearly it is
wg = "Charm"                            

# DecayTreeTuple for D2hhPromptDst2D2KKLine stripping line
#=============================================================================================================================================

# DecayTreeTuple definitions
dtt = DecayTreeTuple("DstDKKPiTuple")

# For MC DST we keep the same Inputs
if Simulation:
    dtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
    
# For data mDST the root is different. We use the RootInTES method to change it
# These RootInTES depends of the stream of each WG. Has to be checked
else:
    dtt.Inputs = ["/Phys/{0}/Particles".format(line)]
    dtt.RootInTES = "/Event/{0}".format(wg)


dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
dtt.addBranches({"Dst": "[D*(2010)+ -> (D0 -> K- K+) pi+]CC", 
                 "Dz" : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC", 
                 "Kmi": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kpl": "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pi" : "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})


# TupleTools addition
tupletools_list =  ["TupleToolEventInfo",   
                    "TupleToolANNPID",      
                    "TupleToolGeometry",    
                    "TupleToolKinematic"]  

dtt.ToolList = tupletools_list

track_tool = dtt.addTupleTool("TupleToolTrackInfo")
track_tool.Verbose = True 

# MC TupleTools only can be run over 
if Simulation:
    MCTruth = dtt.addTupleTool("TupleToolMCTruth")  
    MCTruth.addTupleTool("MCTupleToolHierarchy")    

# LoKi functors addition
dtt_Dst_LoKi = dtt.Dst.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Dst_LoKi")
dtt_Dst_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"}

dtt_Dz_LoKi = dtt.Dz.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Dz_LoKi")
dtt_Dz_LoKi.Variables = {"ETA": "ETA",
                         "PHI": "PHI"}

dtt_Kpl_LoKi = dtt.Kpl.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Kpl_LoKi")
dtt_Kpl_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"}

dtt_Kmi_LoKi = dtt.Kmi.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Kmi_LoKi")
dtt_Kmi_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"} 

dtt_pi_LoKi = dtt.pi.addTupleTool("LoKi::Hybrid::TupleTool/dtt_pi_LoKi")
dtt_pi_LoKi.Variables = {"ETA": "ETA",
                         "PHI": "PHI"}

Dz_ct  = dtt.Dz.addTupleTool("TupleToolPropertime")

"""
We apply the vertex refit through the TupleToolDecayTreeFitter
"""

dtt.Dst.addTupleTool('TupleToolDecayTreeFitter/ConsD')
dtt.Dst.ConsD.constrainToOriginVertex = True            # Vertex contrain
dtt.Dst.ConsD.Verbose                 = True            # All the information
dtt.Dst.ConsD.daughtersToConstrain    = ["D0"]          # Do a constraint to D0 -> K+K- fit
dtt.Dst.ConsD.UpdateDaughters         = True            # Update the tracks for all daughters


"""
To add trigger information we need to use 2 TupleTools
-TupleToolTrigger
-TupleToolTISTOS
The last one is important in order to calculate the trigger efficiency
"""

"""
*Note*: There are a lot of trigger lines for each trigger level
        Not all of them are applied to our data or MC
        We have to check which ones are applied through
"""
trigger_list = [#L0                
                "L0HadronDecision",
                "L0ElectronDecision",
                "L0PhotonDecision",
                #Hlt1 
                "Hlt1TrackMVADecision",
                "Hlt1TwoTrackMVADecision",
                "Hlt1TrackMVATightDecision",
                "Hlt1TwoTrackMVATightDecision",
                #Hlt2 lines TCKsh getHlt2(0x21751801)
                "Hlt2Topo2BodyDecision",
                "Hlt2Topo3BodyDecision",
                "Hlt2Topo4BodyDecision",]


dtt_trigger = dtt.addTupleTool("TupleToolTrigger")
dtt_trigger.Verbose = True
dtt_trigger.TriggerList = trigger_list

dtt_TISTOS = dtt.addTupleTool("TupleToolTISTOS")
dtt_TISTOS.Verbose = True
dtt_TISTOS.TriggerList = trigger_list
#=============================================================================================================================================

# MCDecayTreeTuple for D2hhPromptDst2D2KKLine stripping line
#=============================================================================================================================================
mcdtt = MCDecayTreeTuple("DstDKKPiMCTuple")
mcdtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
mcdtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
mcdtt.addBranches({"Dst": "[D*(2010)+ -> (D0 -> K- K+) pi+]CC", 
                   "Dz" : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC", 
                   "Kmi": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                   "Kpl": "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                   "pi" : "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})

# TupleTools addition
# Only DecayTreeTupleMC TupleTools are valid
mctupletools_list =  ["MCTupleToolKinematic",
                      "MCTupleToolHierarchy",
                      "MCTupleToolEventType",        # EventTypes runned
                      "MCTupleToolInteractions",     # Number of interactions of the MC
                      "TupleToolGeneration",         # Generation information
                      "MCTupleToolAngles"]           # Angular information

mcdtt.ToolList = mctupletools_list
#=============================================================================================================================================

# Primary Vertex check
# We only run the PV checker on MC because it is stored in DST. 'Rec/Vertex/Primary' maybe is not stored in mDST or it is empty
if Simulation:
    pv = LoKi__VoidFilter("hasPV", Code="CONTAINS('Rec/Vertex/Primary')>0")
    gs = GaudiSequencer("myseq")
    gs.Members += [pv, dtt, mcdtt]

    
# DaVinci options
if Simulation:
    DaVinci().UserAlgorithms += [gs]                # Instead of the DecayTreeTuple, we put the sequencer
    DaVinci().InputType  = "DST"                    # DST MC
    DaVinci().DataType   = "2016"                   # Year
    DaVinci().EvtMax     = -1
    # Magnet Conditions 
    DaVinci().CondDBtag  = "sim-20170721-2-vc-md100" 
    DaVinci().DDDBtag    = "dddb-20170721-3"
else:
    DaVinci().UserAlgorithms += [dtt]
    DaVinci().InputType  = "MDST"                   # mDST data
    DaVinci().DataType   = "2018"                   # Year
    DaVinci().EvtMax     = 50000                    # For data, -1 is to much for an easy check
    # Magnet Conditions 
    DaVinci().CondDBtag  = "cond-20180202"          # The magnet properties changed because of 2018 year and data condition
    DaVinci().DDDBtag    = "dddb-20171030-3"

DaVinci().TupleFile  = "advanced_ntuple.root"       # Name of the ntuple 
DaVinci().PrintFreq  = 1000                         # Pritting options
DaVinci().Simulation = Simulation                   # MC or Data 
DaVinci().Lumi       = not DaVinci().Simulation     # Only True for Data


# Read local nTuples
dst_path  = "../DST_files/"
dst_name  = "00070793_00000040_7.AllStreams.dst"
mdst_name = "00077434_00001776_1.charm.mdst"

if Simulation:
    IOHelper().inputFiles([dst_path+dst_name], 
                       clear=True)
else:
    IOHelper().inputFiles([dst_path+mdst_name], 
                       clear=True)