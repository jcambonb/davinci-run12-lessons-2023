"""
StarterKit 2023: DaVinci Run(1/2) Lessons 

How to use DST from the bkk instead of downloading them
The Dst+ -> D0(-> K+K-) pi+ MC DST is used

Ivan Cambon Bouzas
Instituto Galego de Altas Enerxias (IGFAE)
Universidade de Santiago de Compostela

LHCb Collaboration
"""

# Important imports
from Configurables import DecayTreeTuple, DaVinci, LoKi__VoidFilter, GaudiSequencer
from DecayTreeTuple.Configuration import addBranches
from GaudiConf import IOHelper

# Stream and stripping line where our particles are
stream = "AllStreams"
line = "D2hhPromptDst2D2KKLine"

# DecayTreeTuple definitions
dtt = DecayTreeTuple("DstDKKPiTuple")
dtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
dtt.addBranches({"Dst": "[D*(2010)+ -> (D0 -> K- K+) pi+]CC", 
                 "Dz" : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC", 
                 "Kmi": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kpl": "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pi" : "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})


# TupleTools addition
tupletools_list =  ["TupleToolEventInfo",   
                    "TupleToolANNPID",      
                    "TupleToolGeometry",    
                    "TupleToolKinematic"]  

dtt.ToolList = tupletools_list

track_tool = dtt.addTupleTool("TupleToolTrackInfo")
track_tool.Verbose = True 

MCTruth = dtt.addTupleTool("TupleToolMCTruth")  
MCTruth.addTupleTool("MCTupleToolHierarchy")    

# LoKi functors addition
dtt_Dst_LoKi = dtt.Dst.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Dst_LoKi")
dtt_Dst_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"}

dtt_Dz_LoKi = dtt.Dz.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Dz_LoKi")
dtt_Dz_LoKi.Variables = {"ETA": "ETA",
                         "PHI": "PHI"}

dtt_Kpl_LoKi = dtt.Kpl.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Kpl_LoKi")
dtt_Kpl_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"}

dtt_Kmi_LoKi = dtt.Kmi.addTupleTool("LoKi::Hybrid::TupleTool/dtt_Kmi_LoKi")
dtt_Kmi_LoKi.Variables = {"ETA": "ETA",
                          "PHI": "PHI"} 

dtt_pi_LoKi = dtt.pi.addTupleTool("LoKi::Hybrid::TupleTool/dtt_pi_LoKi")
dtt_pi_LoKi.Variables = {"ETA": "ETA",
                         "PHI": "PHI"}

"""
We will add the TupleToolProperTime for the D0, which requires that the decay mothers come from the PV
We have to check if the there is PV to make the algorithms work
For that, we use the following structure
"""

Dz_ct  = dtt.Dz.addTupleTool("TupleToolPropertime")
    
pv = LoKi__VoidFilter("hasPV", Code="CONTAINS('Rec/Vertex/Primary')>0")
gs = GaudiSequencer("myseq")
gs.Members += [pv, dtt]  # The VoidFilter must be made first. The DecayTreeTuple has to be in the end

# DaVinci options
DaVinci().UserAlgorithms += [gs]                # Instead of the DecayTreeTuple, we put the sequencer
DaVinci().InputType  = "DST"                    # DST or mDST  
DaVinci().TupleFile  = "advanced_ntuple.root"   # Name of the ntuple 
DaVinci().PrintFreq  = 1000                     # Pritting options
DaVinci().DataType   = "2016"                   # Year
DaVinci().Simulation = True                     # MC or Data 
DaVinci().Lumi       = not DaVinci().Simulation # Only True for Data
DaVinci().EvtMax     = -1                       # -1 means all events inside the DST file
# Magnet Conditions 
DaVinci().CondDBtag  = "sim-20170721-2-vc-md100" 
DaVinci().DDDBtag    = "dddb-20170721-3"

# Read the DST through catalog

from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs = ["xmlcatalog_file:/catalogs/myCatalog.xml"]



