"""
StarterKit 2023: DaVinci Run(1/2) Lessons 

Adding new variables to our DecayTreeTuple through TupleTools
The Dst+ -> D0(-> K+K-) pi+ MC DST is used

Ivan Cambon Bouzas
Instituto Galego de Altas Enerxias (IGFAE)
Universidade de Santiago de Compostela

LHCb Collaboration
"""

# Important imports
from Configurables import DecayTreeTuple, DaVinci
from DecayTreeTuple.Configuration import addBranches
from GaudiConf import IOHelper

# Stream and stripping line where our particles are
stream = "AllStreams"
line = "D2hhPromptDst2D2KKLine"

# DecayTreeTuple definitions
dtt = DecayTreeTuple("DstDKKPiTuple")
dtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
dtt.addBranches({"Dst": "[D*(2010)+ -> (D0 -> K- K+) pi+]CC", 
                 "Dz" : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC", 
                 "Kmi": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kpl": "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pi" : "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})


# 1st method -> adding tupletools through list: ToolList method

tupletools_list =  ["TupleToolEventInfo",   # Event information (number of primary verteces, number of tracks in VELO, etc)
                    "TupleToolANNPID",      # PID information of the particles (Probability of being pion, etc)
                    "TupleToolGeometry",    # Reconstruction information (vertex chi2, minimum impact parameter (IP) )
                    "TupleToolKinematic"]   # Kinematic information (Energy, momentum, transverse momentum)

dtt.ToolList = tupletools_list

# 2nd method -> adding tupletools one by one: addtupletool method
track_tool = dtt.addTupleTool("TupleToolTrackInfo")
track_tool.Verbose = True  # Some tupletools have specific configurables. We can change it only through this method


"""
MC information
DecayTreeTuple admits MCTupleTools for information regarding the MC simulation process
These TupleTools work for MC DST only, of course.
Allow us to get our simulated signal properties after the whole reconstruction process
"""

MCTruth = dtt.addTupleTool("TupleToolMCTruth")  # We get the kinematic properties before detector reconstruction
MCTruth.addTupleTool("MCTupleToolHierarchy")    # We get the decay mothers and grandmothers of the final state particles

# DaVinci options
DaVinci().UserAlgorithms += [dtt]               # Add algorithms to the nTuple
DaVinci().InputType  = "DST"                    # DST or mDST  
DaVinci().TupleFile  = "basic_ntuple.root"      # Name of the ntuple 
DaVinci().PrintFreq  = 1000                     # Pritting options
DaVinci().DataType   = "2016"                   # Year
DaVinci().Simulation = True                     # MC or Data 
DaVinci().Lumi       = not DaVinci().Simulation # Only True for Data
DaVinci().EvtMax     = -1                       # -1 means all events inside the DST file
# Magnet Conditions 
DaVinci().CondDBtag  = "sim-20170721-2-vc-md100" 
DaVinci().DDDBtag    = "dddb-20170721-3"

# Read local nTuples
dst_path = "../DST_files/"
dst_name = "00070793_00000040_7.AllStreams.dst"

IOHelper().inputFiles([dst_path+dst_name], 
                       clear=True)