"""
StarterKit 2023: DaVinci Run(1/2) Lessons 

Adding branches to our DecayTreeTuple
The Dst+ -> D0(-> K+K-) pi+ MC DST is used

Ivan Cambon Bouzas
Instituto Galego de Altas Enerxias (IGFAE)
Universidade de Santiago de Compostela

LHCb Collaboration
"""

# From now, we will import all the configurables at the beginning
from Configurables import DecayTreeTuple, DaVinci
from DecayTreeTuple.Configuration import addBranches
from GaudiConf import IOHelper

# Stream and stripping line where our particles are
stream = "AllStreams"
line = "D2hhPromptDst2D2KKLine"

# DecayTreeTuple definition
dtt = DecayTreeTuple("DstDKKPiTuple")
dtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
# Now we have to put ^ in each daughter particle
dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
# We put the branches as a dictionary. Each key will be the new branches for each daugther particle
dtt.addBranches({"Dst": "[D*(2010)+ -> (D0 -> K- K+) pi+]CC", 
                 "Dz" : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC", # For intermidiate resonances, we put the ^ before the parenthesis 
                 "Kmi": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kpl": "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pi" : "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})


# DaVinci options
DaVinci().UserAlgorithms += [dtt]               # Add algorithms to the nTuple
DaVinci().InputType  = "DST"                    # DST or mDST  
DaVinci().TupleFile  = "basic_ntuple.root"      # Name of the ntuple 
DaVinci().PrintFreq  = 1000                     # Pritting options
DaVinci().DataType   = "2016"                   # Year
DaVinci().Simulation = True                     # MC or Data 
DaVinci().Lumi       = not DaVinci().Simulation # Only True for Data
DaVinci().EvtMax     = -1                       # -1 means all events inside the DST file
# Magnet Conditions 
DaVinci().CondDBtag  = "sim-20170721-2-vc-md100" 
DaVinci().DDDBtag    = "dddb-20170721-3"

# Read local nTuples
dst_path = "../DST_files/"
dst_name = "00070793_00000040_7.AllStreams.dst"

IOHelper().inputFiles([dst_path+dst_name], 
                       clear=True)